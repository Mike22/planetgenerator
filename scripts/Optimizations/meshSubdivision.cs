﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class meshSubdivision : MonoBehaviour {
    // Use this for initialization
    private List<Vector3> shotPoints;
    private List<Ray> furstrumRays;
    private Vector3 mOldCameraPosition;
    private Vector3 mCentroid;
    private float cTargetDir = 0;

    public int mResolution;
    public float mScale;
    public int LOD = 0;
    QuadTree qt;

    void Start () {
        shotPoints = new List<Vector3>();
        furstrumRays = new List<Ray>();
        //RecomputeRays();
        Vector3[] vects = gameObject.GetComponent<MeshFilter>().mesh.vertices;
        List<Vector3> pVertices = new List<Vector3>();

        pVertices.Add(gameObject.GetComponent<MeshFilter>().mesh.vertices[0]);
        pVertices.Add(gameObject.GetComponent<MeshFilter>().mesh.vertices[mResolution - 1]);
        pVertices.Add(gameObject.GetComponent<MeshFilter>().mesh.vertices[((mResolution * mResolution) - mResolution)]);
        pVertices.Add(gameObject.GetComponent<MeshFilter>().mesh.vertices[(mResolution * mResolution) - 1]);

        Vector3 centroid = Vector3.zero;
        centroid = Vector3.Slerp(pVertices[0], pVertices[3], 0.5f);
        float distance = Vector3.Distance(centroid, Camera.main.transform.position);

        qt = new QuadTree(0, gameObject, mResolution, mScale, distance);
    }

    private void LodUpdate()
    {

        Vector3 gameObjectDirection = Camera.main.WorldToScreenPoint(gameObject.GetComponent<Renderer>().bounds.center);
        List<Vector3> pVertices = new List<Vector3>();

        pVertices.Add(gameObject.GetComponent<MeshFilter>().mesh.vertices[0]);
        pVertices.Add(gameObject.GetComponent<MeshFilter>().mesh.vertices[mResolution - 1]);
        pVertices.Add(gameObject.GetComponent<MeshFilter>().mesh.vertices[((mResolution * mResolution) - mResolution)]);
        pVertices.Add(gameObject.GetComponent<MeshFilter>().mesh.vertices[(mResolution * mResolution) - 1]);

        Vector3 centroid = Vector3.zero;
        centroid = Vector3.Slerp(pVertices[0], pVertices[3], 0.5f);

        //if (Vector3.Distance(centroid, Camera.main.transform.position) < mScale) {
            qt.Traverse();
        //}

    }

    private void OnDrawGizmos()
    {
        List<Vector3> pVertices = new List<Vector3>();

        pVertices.Add(gameObject.GetComponent<MeshFilter>().mesh.vertices[0]);
        pVertices.Add(gameObject.GetComponent<MeshFilter>().mesh.vertices[mResolution - 1]);
        pVertices.Add(gameObject.GetComponent<MeshFilter>().mesh.vertices[((mResolution * mResolution) - mResolution)]);
        pVertices.Add(gameObject.GetComponent<MeshFilter>().mesh.vertices[(mResolution * mResolution) - 1]);

        Vector3 centroid = Vector3.zero;
        centroid = Vector3.Slerp(pVertices[0], pVertices[3], 0.5f);

        foreach (Vector3 pt in pVertices)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(pt, 1f);
        }
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(centroid + centroid.normalized, 1f);
    }
    /*private void OnDrawGizmos()
    {
        Vector3[] verts = gameObject.GetComponent<MeshFilter>().sharedMesh.vertices;
        
        for (int i = 0;  i < verts.Length-mResolution; i++)
        {
            cTargetDir = Vector3.Dot(gameObject.GetComponent<MeshFilter>().sharedMesh.normals[i] + verts[i], Camera.main.transform.position - verts[i]);
            if (cTargetDir < 0) continue;
            List<Vector3> vertices = new List<Vector3>(4);
            List<Vector3> normals = new List<Vector3>(4);

            vertices.Add(verts[i]);
            vertices.Add(verts[i+1]);
            vertices.Add(verts[i+ (mResolution-1)]);
            vertices.Add(verts[i+ (mResolution-1) + 1]);

            normals.Add(gameObject.GetComponent<MeshFilter>().sharedMesh.normals[i]);
            normals.Add(gameObject.GetComponent<MeshFilter>().sharedMesh.normals[i+1]);
            normals.Add(gameObject.GetComponent<MeshFilter>().sharedMesh.normals[i+ (mResolution - 1)]);
            normals.Add(gameObject.GetComponent<MeshFilter>().sharedMesh.normals[i+ (mResolution - 1) + 1]);


            if (PointsInRange(vertices, normals))
            {
                Gizmos.color = Color.black;
                Gizmos.DrawSphere(transform.TransformPoint(vertices[0]), 0.1f);
                Gizmos.DrawSphere(transform.TransformPoint(vertices[1]), 0.1f);
                Gizmos.DrawSphere(transform.TransformPoint(vertices[2]), 0.1f);
                Gizmos.DrawSphere(transform.TransformPoint(vertices[3]), 0.1f);
                
            }
        }
    }*/
    private bool ObjectHasTransformed(Vector3 v1, Vector3 v2)
    {
        bool vectorDistance = false;

        vectorDistance = v1.Equals(v2);
        return vectorDistance;
    }

    private bool PointsInRange()
    {
        // If any of the quad points is in the viewport,
        // then we've got an intercection

        // TODO: Analyze the this function

        Vector3[] points = gameObject.GetComponent<MeshFilter>().mesh.vertices;
        Vector3[] normals = gameObject.GetComponent<MeshFilter>().mesh.normals;

        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);
        Bounds rendererBounds = gameObject.GetComponent<MeshRenderer>().bounds;
        for (var i=0; i<points.Length; ++i)
        {
            Vector3 worldPoint = transform.TransformPoint(points[i]);
            cTargetDir = Vector3.Dot(normals[i] + points[i], Camera.main.transform.position - points[i]);
            if (cTargetDir < 0) continue;

            /*Vector3 target = AngleSigned(worldPoint);

            if (target.x > 1.0f ||
                target.x < 0.0f ||
                target.y > 1.0f ||
                target.y < 0.0f)
            {
                return false;
            }*/

            if (GeometryUtility.TestPlanesAABB(planes, rendererBounds))
            {
                return true;
            }

        }
        // interception found
        
        return true;
    }

    private Vector3 AngleSigned(Vector3 targetPoint)
    {
        Vector3 targetDir = (targetPoint - Camera.main.transform.position);
        //angleText.text = Camera.main.WorldToViewportPoint(targetPoint).ToString();
        return Camera.main.WorldToViewportPoint(targetPoint);
    }

    private float[] DefineRadiuses()
    {
        float[] radiuses = new float[2];
        List<float> centroidDistances = new List<float>(shotPoints.Count);
        float minDistance = 0f;
        float maxDistance = 0f;

        Vector3 centroid = GetCentroid();
        foreach (Vector3 shotPoint in shotPoints)
        {
            // get distances per point for centroid
            centroidDistances.Add(Vector3.Distance(centroid, shotPoint));
        }

        minDistance = Mathf.Min(centroidDistances.ToArray());
        maxDistance = Mathf.Max(centroidDistances.ToArray());
        
        radiuses[0] = minDistance;
        radiuses[1] = maxDistance;

        return radiuses;
    }

    private Vector3 GetCentroid()
    {
        Vector3 centroid = Vector3.zero;

        if (shotPoints != null && shotPoints.Count > 0)
        {

            foreach (Vector3 shotPoint in shotPoints)
            {
                centroid.x += shotPoint.x;
                centroid.y += shotPoint.y;
                centroid.z += shotPoint.z;
            }

            centroid.x /= 4f;
            centroid.y /= 4f;
            centroid.z /= 4f;
            var counter = 0;
            foreach (Vector3 shotPoint in shotPoints)
            {
                // get distances per point for centroid
                Vector3.Distance(centroid, shotPoint);
                ++counter;
            }
        }

        
        return centroid;
    }


    private void RecomputeRays()
    {
        furstrumRays.Clear();
        shotPoints.Clear();

        RaycastHit hit;
        Ray ray0 = Camera.main.ViewportPointToRay(new Vector3(0.0f, 0.0f, 0.0f));
        Ray ray1 = Camera.main.ViewportPointToRay(new Vector3(0.0f, 1.0f, 0.0f));
        Ray ray2 = Camera.main.ViewportPointToRay(new Vector3(1.0f, 0.0f, 0.0f));
        Ray ray3 = Camera.main.ViewportPointToRay(new Vector3(1.0f, 1.0f, 0.0f));
        
        furstrumRays.Add(ray0);
        furstrumRays.Add(ray1);
        furstrumRays.Add(ray2);
        furstrumRays.Add(ray3);

        mOldCameraPosition = Camera.main.transform.position;
        foreach (Ray ray in furstrumRays)
        {
            if (Physics.Raycast(ray, out hit))
            {
                // Do something with the object that was hit by the raycast.
                // TODO: Investigate if it's more efficient to calculate the distance from the main camera
                // manually or by a Raycast
                // Transform objectHit = hit.transform; // TODO: Object to be manipulated
                
                shotPoints.Add(hit.point);
            }
        }
    }
    // Update is called once per frame
    void Update () {

        if (!ObjectHasTransformed(Camera.main.transform.position, mOldCameraPosition)) {
            // TODO: add triangulation math here
            mOldCameraPosition = Camera.main.transform.position;
            Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);

            LodUpdate();
        }
    }
}
