﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuadTree
{
    private List<Vector3> vertices;
    private int leafNumber;
    private int mResolution;
    private int LOD;
    private GameObject mGameObject;
    private List<QuadTree> mChildren;
    private Dictionary<float, int> lodMap;
    private float mDistance;
    private float mScale;
    private bool active;

    public QuadTree()
    {
        // default constructor
    }

    public QuadTree(int LOD, GameObject gObject, int resolution, float scale, float distance)
    {
        vertices = new List<Vector3>(gObject.GetComponent<MeshFilter>().mesh.vertices);
        leafNumber = 0;
        active = false;
        mGameObject = gObject;
        mResolution = resolution;
        mScale = scale;
        mChildren = null;
        mDistance = distance;
        this.LOD = LOD;
        lodMap = new Dictionary<float, int>();
        float s = 24901f;
        lodMap.Add(s, 1);
        s /= 2f;
        lodMap.Add(s, 2);
        s /= 2f;
        lodMap.Add(s, 3);
        s /= 2f;
        lodMap.Add(s, 4);
        s /= 2f;
        lodMap.Add(s, 5);

    }

    private int GetLOD (float distance)
    {
        int lowestPossibleLod = 0;
        foreach (KeyValuePair<float, int> mappedDistance in lodMap)
        {
            //Debug.Log(distance + "<" + mappedDistance.Key);
            if (distance < mappedDistance.Key)
            {
                lowestPossibleLod = mappedDistance.Value;
            }
        }
        return lowestPossibleLod;
    }

    private Mesh CopyVertices(int subDivisionIndex)
    {
        Mesh m = new Mesh();

        int newRes = ((mResolution - 1) / 2) + 1;
        List<Vector3> chunkVectors = new List<Vector3>((newRes) * (newRes));
        List<Vector3> chunkNormals = new List<Vector3>((newRes) * (newRes));

        int iterator = 0;

        int j = subDivisionIndex;
        List<int> chunkTriangles = new List<int>((newRes - 1) * (newRes - 1) * 6);
        for (int i = 0; i < newRes * newRes; ++i)
        {
            
            chunkVectors.Add(vertices[mResolution * iterator + j]);
            chunkNormals.Add(mGameObject.GetComponent<MeshFilter>().mesh.normals[mResolution * iterator + j]);
            if (j > subDivisionIndex && j % (newRes-1) == 0)
            {
                
                ++iterator;
                j = subDivisionIndex;
            }
            else
            {
                ++j;
            }
        }
        int triangeIndex = 0;
        for (int i = 0, step = 1; i < (newRes - 1) * (newRes - 1) * 6; i += 6)
        {
            chunkTriangles.Add(triangeIndex + newRes);
            chunkTriangles.Add(triangeIndex + 1);
            chunkTriangles.Add(triangeIndex);

            chunkTriangles.Add(triangeIndex + newRes);
            chunkTriangles.Add(triangeIndex + (newRes + 1));
            chunkTriangles.Add(triangeIndex + 1);

            if (triangeIndex > 0 && (triangeIndex + 1) % (((newRes - 1) * step) + (step - 1)) == 0)
            {
                ++step;
                triangeIndex += 2;
            }
            else
            {
                ++triangeIndex;
            }

        }

        m.vertices = chunkVectors.ToArray();
        m.normals = chunkNormals.ToArray();
        m.triangles = chunkTriangles.ToArray();

        Mesh tempMesh = new Mesh();
        tempMesh = MeshDetail(m);
        m.vertices = tempMesh.vertices;
        m.normals = tempMesh.normals;
        m.triangles = tempMesh.triangles;
        return m;
    }

    private Mesh MeshDetail(Mesh pMesh)
    {
        Mesh mesh = new Mesh();
        List<Vector3> verts = new List<Vector3>(mResolution * mResolution);
        List<Vector3> normals = new List<Vector3>(mResolution * mResolution);
        int [] triIndex = new int[(mResolution-1) * (mResolution-1) * 6];
        int newRes = ((mResolution - 1) / 2) + 1;

        Vector3[] generatedVertices = pMesh.vertices;
        Vector3[] generatedNormals = pMesh.normals;
        int row =  1;
        int newPoints = 0;
        for (int i = 0; i < newRes * newRes; i++)
        {
            
            if (i==0 || i % newRes * row != 0) {
                ++newPoints;
                verts.Add(generatedVertices[i]);
                normals.Add(generatedNormals[i]);
            }

            if (i==0 || i % (newRes*row-1) != 0 && i % newRes != 0)
            {
                ++newPoints;
                verts.Add(Vector3.Slerp(generatedVertices[i], generatedVertices[i + 1], 0.5f));
                normals.Add(Vector3.Slerp(generatedNormals[i], generatedNormals[i + 1], 0.5f).normalized);
            }

            if (i>0 && i % newRes == 0)
            {
                for (int k=0; k<newRes; ++k) {
                    // do between columns
                    verts.Add(Vector3.Slerp(generatedVertices[(i+k)-newRes], generatedVertices[i+k], 0.5f));
                    normals.Add(Vector3.Slerp(generatedNormals[(i + k) - newRes], generatedNormals[i + k], 0.5f).normalized);
                    if (k < newRes-1)
                    {
                        Vector3 baseMid = Vector3.Slerp(generatedVertices[(i + k) - newRes], generatedVertices[(i + k + 1) - newRes], 0.5f);
                        Vector3 lowerMiddle = Vector3.Slerp(generatedVertices[(i + k)],
                                                           generatedVertices[(i + k) + 1], 0.5f);
                        verts.Add(Vector3.Slerp(baseMid, lowerMiddle, 0.5f));
                        normals.Add(Vector3.Slerp(baseMid, lowerMiddle, 0.5f).normalized);
                    }
                }
                verts.Add(generatedVertices[i]);
                normals.Add(generatedNormals[i]);

                verts.Add(Vector3.Slerp(generatedVertices[i], generatedVertices[i + 1], 0.5f));
                normals.Add(Vector3.Slerp(generatedVertices[i], generatedVertices[i + 1], 0.5f).normalized);
            }

            if (i>0 && (i+1) % newRes == 0)
            {
                ++row;
            }
        }
        
        int triangeIndex = 0;

        for (int i = 0, step = 1; i < triIndex.Length; i += 6)
        {
            triIndex[i] = triangeIndex + mResolution;
            triIndex[i + 1] = triangeIndex + 1;
            triIndex[i + 2] = triangeIndex;

            triIndex[i + 3] = triangeIndex + mResolution;
            triIndex[i + 4] = triangeIndex + (mResolution + 1);
            triIndex[i + 5] = triangeIndex + 1;

            if (triangeIndex > 0 && (triangeIndex + 1) % (((mResolution-1) * step) + (step - 1)) == 0)
            {
                ++step;
                triangeIndex += 2;
            }
            else
            {
                ++triangeIndex;
            }

        }
        mesh.vertices = verts.ToArray();
        mesh.normals = normals.ToArray();
        mesh.triangles = triIndex;
        mesh.RecalculateBounds();

        return mesh;
    }


    private void Subdivide () {
        int count = 4;
        int subDivisionIndex = 0;
        
        mChildren = new List<QuadTree>();
        while (count > 0)
        {
            GameObject childObject = new GameObject();
            Mesh m = new Mesh();
            childObject.transform.parent = mGameObject.transform; // set as child of parent object
            childObject.AddComponent<MeshFilter>();
            childObject.AddComponent<MeshRenderer>();

            switch (count)
            {
                case 1:
                    subDivisionIndex = 0;
                    break;
                case 2:
                    subDivisionIndex = (mResolution-1)/2;
                    break;
                case 3:
                    subDivisionIndex = ((mResolution * mResolution)-mResolution)/2;
                    break;
                case 4:
                    subDivisionIndex = (((mResolution * mResolution) - mResolution) / 2)+((mResolution-1)/2);
                    break;
            }

            m = CopyVertices(subDivisionIndex);
            childObject.name = LOD + "_" + subDivisionIndex.ToString();

            childObject.GetComponent<MeshFilter>().mesh.vertices = m.vertices;
            childObject.GetComponent<MeshFilter>().mesh.normals = m.normals;
            childObject.GetComponent<MeshFilter>().mesh.triangles = m.triangles;

            childObject.GetComponent<MeshRenderer>().sharedMaterial = mGameObject.GetComponent<MeshRenderer>().sharedMaterial;

            QuadTree qt = new QuadTree(LOD, childObject, mResolution, mScale, mDistance);
            mChildren.Add(qt);
            --count;
        }
    }


    public void Traverse ()
    {
        List<Vector3> nodeBoundVertices = new List<Vector3>();
        List<Vector3> nodeBoundNormals = new List<Vector3>();

        Vector3[] tempVertices = mGameObject.GetComponent<MeshFilter>().mesh.vertices;
        Vector3[] tempNormals = mGameObject.GetComponent<MeshFilter>().mesh.normals;

        nodeBoundVertices.Add(tempVertices[0]);
        nodeBoundVertices.Add(tempVertices[mResolution - 1]);
        nodeBoundVertices.Add(tempVertices[(mResolution * mResolution) - mResolution]);
        nodeBoundVertices.Add(tempVertices[(mResolution * mResolution) - 1]);

        nodeBoundNormals.Add(tempNormals[0]);
        nodeBoundNormals.Add(tempNormals[mResolution - 1]);
        nodeBoundNormals.Add(tempNormals[(mResolution * mResolution) - mResolution]);
        nodeBoundNormals.Add(tempNormals[(mResolution * mResolution) - 1]);

        Vector3 centroid = Vector3.zero;
        centroid = Vector3.Slerp(nodeBoundVertices[0], nodeBoundVertices[3], 0.5f);

        mDistance = Vector3.Distance(centroid, Camera.main.transform.position);
        
        if (mChildren == null) {
            // Check if the chunk is in range, and then subdivide it if it is
            if (LOD <= GetLOD(mDistance) && GetLOD(mDistance) < 5)
            {
                ++LOD;
                Subdivide();
                // Make sure you disable the chink's renderere everytime you subdivide
                mGameObject.GetComponent<Renderer>().enabled = false;
            }
        }
        else
        {
            foreach (QuadTree child in mChildren)
            {
                List<Vector3> childBoundVertices = new List<Vector3>();
                List<Vector3> childBoundNormals = new List<Vector3>();
                Vector3[] cTempVertices = child.mGameObject.GetComponent<MeshFilter>().mesh.vertices;
                Vector3[] cTempNormals = child.mGameObject.GetComponent<MeshFilter>().mesh.normals;

                childBoundVertices.Add(cTempVertices[0]);
                childBoundVertices.Add(cTempVertices[mResolution - 1]);
                childBoundVertices.Add(cTempVertices[(mResolution * mResolution) - mResolution]);
                childBoundVertices.Add(cTempVertices[(mResolution * mResolution) - 1]);

                childBoundNormals.Add(cTempNormals[0]);
                childBoundNormals.Add(cTempNormals[mResolution - 1]);
                childBoundNormals.Add(cTempNormals[(mResolution * mResolution) - mResolution]);
                childBoundNormals.Add(cTempNormals[(mResolution * mResolution) - 1]);

                Vector3 childCentroid = Vector3.zero;
                childCentroid = Vector3.Slerp(childBoundVertices[0], childBoundVertices[3], 0.5f);
                float dist = Vector3.Distance(childCentroid, Camera.main.transform.position);

                if (LOD > child.GetLOD(dist) && child.mChildren != null && child.mChildren.Count == 4)
                {
                    child.DetachChildren(child.mChildren);
                    child.mChildren = null;

                    // Make sure you enable the chink's renderere everytime you detach its children
                    child.mGameObject.GetComponent<Renderer>().enabled = true;
                }
                else { 
                    child.Traverse();
                }

            }
        }
    }

    private void DetachChildren(List<QuadTree> pChildren)
    {
        --LOD;
        for (int i=0; i< pChildren.Count; ++i)
        {
            if (pChildren[i].mChildren != null)
            {
                DetachChildren(pChildren[i].mChildren);
            }
            GameObject.Destroy(pChildren[i].mGameObject);
            pChildren[i] = null;
        }
        pChildren = null;
    }

    private bool PointsInRange(List<Vector3> pVertices, List<Vector3> pNormals)
    {
        // If any of the quad points is in the viewport,
        // then we've got an intercection

        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);

        Vector3 centroid = Vector3.zero;
        centroid = Vector3.Slerp(pVertices[0], pVertices[3], 0.5f);
        

        float dist = Vector3.Distance(centroid, Camera.main.transform.position);

        if (LOD > 0 && LOD <= GetLOD(dist))
        {
            return true;
        }

        return true;
    }

    private Vector3 AngleSigned(Vector3 targetPoint)
    {
        Vector3 targetDir = (targetPoint - Camera.main.transform.position);
        return Camera.main.WorldToViewportPoint(targetPoint);
    }
    ~QuadTree ()
    {
        //Debug.Log("destroyed");
    }
}
