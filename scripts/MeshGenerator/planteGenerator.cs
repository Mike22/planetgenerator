﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class planteGenerator : MonoBehaviour {
    [Range(1f, 24901f)]
    public float mScale;
    [Range(1, 100)]
    public int mResolution = 1;
    private Mesh mMesh;
    private MeshFilter mMeshFilter;
    private MeshRenderer mMeshRenderer;
    private float mOldScale = 1.0f;
    private float mOldResolution = 1;
    Dictionary<string, Vector3> directions;

    public Material planetMaterial;

    // Use this for initialization
    void Start () {
        directions = new Dictionary<string, Vector3>();
        directions.Add("up", Vector3.up);
        directions.Add("down", Vector3.down);
        directions.Add("left", Vector3.left);
        directions.Add("right", Vector3.right);
        directions.Add("front", Vector3.forward);
        directions.Add("back", Vector3.back);
        generatePlanet();
	}
	
	// Update is called once per frame
	void Update () {
        /*if (!Mathf.Approximately(mScale, mOldScale) || !Mathf.Approximately(mResolution, mOldResolution))
        {
            Debug.Log("hey");
            gameObject.GetComponent<MeshFilter>().mesh.Clear();
            gameObject.GetComponent<MeshFilter>().mesh = generateFace();
            mOldScale = mScale;
            mOldResolution = mResolution;
        }*/
    }

    private void generatePlanet() {
        gameObject.AddComponent<MeshFilter>();
        gameObject.AddComponent<MeshRenderer>();
        gameObject.GetComponent<MeshRenderer>().sharedMaterial = planetMaterial;

        foreach(KeyValuePair<string, Vector3> direction in directions)
        {
            Mesh mesh = generateFace(direction);
            GameObject gameObj = new GameObject();
            gameObj.AddComponent<MeshFilter>();
            gameObj.AddComponent<MeshRenderer>();

            gameObj.GetComponent<MeshFilter>().mesh = mesh;
            gameObj.GetComponent<MeshRenderer>().sharedMaterial = planetMaterial;
            gameObj.transform.SetParent(gameObject.transform);

            gameObj.name = direction.Key;

            gameObj.AddComponent<meshSubdivision>();
            gameObj.GetComponent<meshSubdivision>().mResolution = mResolution+1;
            gameObj.GetComponent<meshSubdivision>().mScale = mScale;
        }
    }

    private Mesh generateFace(KeyValuePair<string, Vector3> direction) {
        int meshResolution = (int)Mathf.Pow(mResolution + 1.0f, 2.0f);
        Mesh mesh = new Mesh();
        Vector3[] vertices = new Vector3[meshResolution];
        Vector3[] normals = new Vector3[meshResolution];
        int[] triIndex = new int[(mResolution) * (mResolution) * 6];

        float horizontalIndex = 0f;
        float verticalIndex = 0f;
        for (int i = 0;  i < vertices.Length; i++) {
            float computedHorizontal = (horizontalIndex / mResolution);
            float computedVertical = (verticalIndex / mResolution);
            switch (direction.Key) {
                case "up":
                    vertices[i] = new Vector3((computedHorizontal * mScale) - (mScale * 0.5f), mScale * 0.5f, (computedVertical * mScale) - (mScale * 0.5f));
                    break;
                case "down":
                    vertices[i] = new Vector3((computedVertical * mScale) - (mScale * 0.5f), -mScale * 0.5f, (computedHorizontal * mScale) - (mScale * 0.5f));
                    break;
                case "left":
                    vertices[i] = new Vector3(-mScale * 0.5f, (computedHorizontal * mScale) - (mScale * 0.5f), (computedVertical * mScale) - (mScale * 0.5f));
                    break;
                case "right":
                    vertices[i] = new Vector3(mScale * 0.5f, (computedVertical * mScale) - (mScale * 0.5f), (computedHorizontal * mScale) - (mScale * 0.5f));
                    break;
                case "front":
                    vertices[i] = new Vector3((computedVertical * mScale) - (mScale * 0.5f), (computedHorizontal * mScale) - (mScale * 0.5f), mScale * 0.5f);
                    break;
                case "back":
                    vertices[i] = new Vector3((computedHorizontal * mScale) - (mScale * 0.5f), (computedVertical * mScale) - (mScale * 0.5f), -mScale * 0.5f);
                    break;
            }
            
            normals[i] = vertices[i].normalized;
            vertices[i] = normals[i] * mScale;
            if (i > 0 && i < meshResolution && (i + 1) % (mResolution + 1) == 0) {
                horizontalIndex = 0;
                ++verticalIndex;
            }
            else {
                ++horizontalIndex;
            }
        }
        int triangeIndex = 0;
        for (int i=0, step=1; i<triIndex.Length; i+=6) {

            triIndex[i] = triangeIndex + mResolution + 1;
            triIndex[i + 1] = triangeIndex + 1;
            triIndex[i + 2] = triangeIndex;

            triIndex[i + 3] = triIndex[i];
            triIndex[i + 4] = triangeIndex + (mResolution + 2);
            triIndex[i + 5] = triIndex[i + 1];

            if (triangeIndex > 0 && (triangeIndex + 1) % ((mResolution * step) + (step - 1)) == 0) {
                ++step;
                triangeIndex += 2;
            }
            else {
                ++triangeIndex;
            }
        }

        mesh.vertices = vertices;
        mesh.normals = normals;
        mesh.triangles = triIndex;
        mesh.RecalculateBounds();

        return mesh;
    }
}
